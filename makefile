WORKDIR = `pwd`

CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres

INC = -Iinclude -Isrc/GameScene -Iinclude -Isrc
CFLAGS_DEBUG = -g
CFLAGS = -Wall -fexceptions -std=c++14 $(CFLAGS_DEBUG) -fPIC
RESINC =
LIBDIR =
LIB = -Llib -lIrrlicht -lGL -lX11 -lXxf86vm
LDFLAGS =

OBJDIR = obj
OUT = bin/game_cpp
OUT_LIB = lib/libseriousgame.so

OBJ_LIB = $(OBJDIR)/src/GameScene/TaskObjects/TaskObject3.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject4.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject5.o $(OBJDIR)/src/GameScene/TaskObjectsFactory.o $(OBJDIR)/src/Logger.o $(OBJDIR)/src/Shadow.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject2.o $(OBJDIR)/src/TaskBase.o $(OBJDIR)/src/TextureManager.o $(OBJDIR)/src/main.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObject.o $(OBJDIR)/src/Exceptions/ConfigReadException.o $(OBJDIR)/src/Exceptions/SceneryReadException.o $(OBJDIR)/src/GameCFG.o $(OBJDIR)/src/GameEventReceiver.o $(OBJDIR)/src/GamePlayer.o $(OBJDIR)/src/GameScene.o $(OBJDIR)/src/App.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectBarrel.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectPlant.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectWall.o $(OBJDIR)/src/GameScene/StaticObjectsFactory.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject1.o
OBJ_USER_TASKS = $(OBJDIR)/src/StudentTasks/Task1.o $(OBJDIR)/src/StudentTasks/Task2.o $(OBJDIR)/src/StudentTasks/Task3.o $(OBJDIR)/src/StudentTasks/Task4.o $(OBJDIR)/src/StudentTasks/Task5.o
OBJ = $(OBJ_LIB) $(OBJ_USER_TASKS)
#OBJ = $(OBJDIR)/src/GameScene/TaskObjects/TaskObject3.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject4.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject5.o $(OBJDIR)/src/GameScene/TaskObjectsFactory.o $(OBJDIR)/src/Logger.o $(OBJDIR)/src/Shadow.o $(OBJDIR)/src/StudentTasks/Task1.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject2.o $(OBJDIR)/src/StudentTasks/Task2.o $(OBJDIR)/src/StudentTasks/Task3.o $(OBJDIR)/src/StudentTasks/Task4.o $(OBJDIR)/src/StudentTasks/Task5.o $(OBJDIR)/src/TaskBase.o $(OBJDIR)/src/TextureManager.o $(OBJDIR)/src/main.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObject.o $(OBJDIR)/src/Exceptions/ConfigReadException.o $(OBJDIR)/src/Exceptions/SceneryReadException.o $(OBJDIR)/src/GameCFG.o $(OBJDIR)/src/GameEventReceiver.o $(OBJDIR)/src/GamePlayer.o $(OBJDIR)/src/GameScene.o $(OBJDIR)/src/App.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectBarrel.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectPlant.o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectWall.o $(OBJDIR)/src/GameScene/StaticObjectsFactory.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject.o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject1.o

all: check_dirs $(OBJ_USER_TASKS)
	$(LD) $(LIBDIR) -o $(OUT) $(OBJ_USER_TASKS) $(LDFLAGS) $(LIB) -lseriousgame
	cp lib/* bin/
	mv bin/libIrrlicht.so bin/libIrrlicht.so.1.8

clean:
	rm -f $(OBJ) $(OUT)
	rm -rf bin
	rm -rf $(OBJDIR)

check_dirs:
	test -d bin || mkdir -p bin
	test -d $(OBJDIR)/src/GameScene/TaskObjects || mkdir -p $(OBJDIR)/src/GameScene/TaskObjects
	test -d $(OBJDIR)/src/GameScene || mkdir -p $(OBJDIR)/src/GameScene
	test -d $(OBJDIR)/src || mkdir -p $(OBJDIR)/src
	test -d $(OBJDIR)/src/StudentTasks || mkdir -p $(OBJDIR)/src/StudentTasks
	test -d $(OBJDIR)/src/GameScene/StaticObjects || mkdir -p $(OBJDIR)/src/GameScene/StaticObjects
	test -d $(OBJDIR)/src/Exceptions || mkdir -p $(OBJDIR)/src/Exceptions

lib: check_dirs $(OBJ)
	$(LD) -shared -o $(OUT_LIB) -fPIC $(OBJ_LIB) $(LIB)

dist: lib all
	test -d dist || mkdir -p dist
	cp -R bin dist/
	cp -R include dist/
	cp -R media dist/
	cp config.txt dist/
	test -d dist/src || mkdir -p dist/src
	cp -R src/StudentTasks dist/src/
	cp makefile dist/
	cp src/TaskBase.hpp dist/src/
	cp -R lib dist/
	cp -R doc dist/

doc:
	doxygen src/Doxyfile

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject3.o: src/GameScene/TaskObjects/TaskObject3.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject3.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject3.o

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject4.o: src/GameScene/TaskObjects/TaskObject4.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject4.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject4.o

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject5.o: src/GameScene/TaskObjects/TaskObject5.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject5.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject5.o

$(OBJDIR)/src/GameScene/TaskObjectsFactory.o: src/GameScene/TaskObjectsFactory.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjectsFactory.cpp -o $(OBJDIR)/src/GameScene/TaskObjectsFactory.o

$(OBJDIR)/src/Logger.o: src/Logger.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/Logger.cpp -o $(OBJDIR)/src/Logger.o

$(OBJDIR)/src/Shadow.o: src/Shadow.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/Shadow.cpp -o $(OBJDIR)/src/Shadow.o

$(OBJDIR)/src/StudentTasks/Task1.o: src/StudentTasks/Task1.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/StudentTasks/Task1.cpp -o $(OBJDIR)/src/StudentTasks/Task1.o

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject2.o: src/GameScene/TaskObjects/TaskObject2.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject2.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject2.o

$(OBJDIR)/src/StudentTasks/Task2.o: src/StudentTasks/Task2.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/StudentTasks/Task2.cpp -o $(OBJDIR)/src/StudentTasks/Task2.o

$(OBJDIR)/src/StudentTasks/Task3.o: src/StudentTasks/Task3.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/StudentTasks/Task3.cpp -o $(OBJDIR)/src/StudentTasks/Task3.o

$(OBJDIR)/src/StudentTasks/Task4.o: src/StudentTasks/Task4.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/StudentTasks/Task4.cpp -o $(OBJDIR)/src/StudentTasks/Task4.o

$(OBJDIR)/src/StudentTasks/Task5.o: src/StudentTasks/Task5.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/StudentTasks/Task5.cpp -o $(OBJDIR)/src/StudentTasks/Task5.o

$(OBJDIR)/src/TaskBase.o: src/TaskBase.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/TaskBase.cpp -o $(OBJDIR)/src/TaskBase.o

$(OBJDIR)/src/TextureManager.o: src/TextureManager.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/TextureManager.cpp -o $(OBJDIR)/src/TextureManager.o

$(OBJDIR)/src/main.o: src/main.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/main.cpp -o $(OBJDIR)/src/main.o

$(OBJDIR)/src/GameScene/StaticObjects/StaticObject.o: src/GameScene/StaticObjects/StaticObject.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/StaticObjects/StaticObject.cpp -o $(OBJDIR)/src/GameScene/StaticObjects/StaticObject.o

$(OBJDIR)/src/Exceptions/ConfigReadException.o: src/Exceptions/ConfigReadException.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/Exceptions/ConfigReadException.cpp -o $(OBJDIR)/src/Exceptions/ConfigReadException.o

$(OBJDIR)/src/Exceptions/SceneryReadException.o: src/Exceptions/SceneryReadException.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/Exceptions/SceneryReadException.cpp -o $(OBJDIR)/src/Exceptions/SceneryReadException.o

$(OBJDIR)/src/GameCFG.o: src/GameCFG.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameCFG.cpp -o $(OBJDIR)/src/GameCFG.o

$(OBJDIR)/src/GameEventReceiver.o: src/GameEventReceiver.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameEventReceiver.cpp -o $(OBJDIR)/src/GameEventReceiver.o

$(OBJDIR)/src/GamePlayer.o: src/GamePlayer.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GamePlayer.cpp -o $(OBJDIR)/src/GamePlayer.o

$(OBJDIR)/src/GameScene.o: src/GameScene.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene.cpp -o $(OBJDIR)/src/GameScene.o

$(OBJDIR)/src/App.o: src/App.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/App.cpp -o $(OBJDIR)/src/App.o

$(OBJDIR)/src/GameScene/StaticObjects/StaticObjectBarrel.o: src/GameScene/StaticObjects/StaticObjectBarrel.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/StaticObjects/StaticObjectBarrel.cpp -o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectBarrel.o

$(OBJDIR)/src/GameScene/StaticObjects/StaticObjectPlant.o: src/GameScene/StaticObjects/StaticObjectPlant.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/StaticObjects/StaticObjectPlant.cpp -o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectPlant.o

$(OBJDIR)/src/GameScene/StaticObjects/StaticObjectWall.o: src/GameScene/StaticObjects/StaticObjectWall.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/StaticObjects/StaticObjectWall.cpp -o $(OBJDIR)/src/GameScene/StaticObjects/StaticObjectWall.o

$(OBJDIR)/src/GameScene/StaticObjectsFactory.o: src/GameScene/StaticObjectsFactory.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/StaticObjectsFactory.cpp -o $(OBJDIR)/src/GameScene/StaticObjectsFactory.o

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject.o: src/GameScene/TaskObjects/TaskObject.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject.o

$(OBJDIR)/src/GameScene/TaskObjects/TaskObject1.o: src/GameScene/TaskObjects/TaskObject1.cpp
	$(CXX) $(CFLAGS) $(INC) -c src/GameScene/TaskObjects/TaskObject1.cpp -o $(OBJDIR)/src/GameScene/TaskObjects/TaskObject1.o


.PHONY: check_dirs clean
