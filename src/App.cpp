#include <chrono>
#include <thread>

#include "App.hpp"
#include "TextureManager.hpp"
#include "GameCFG.hpp"
#include "Exceptions/ConfigReadException.hpp"
#include "Logger.hpp"

App::App()
{
    try{
        gc = new GameCFG("config.txt");
    }catch(ConfigReadException& ex){
        std::cout << ex.what() << std::endl;
        std::cout << "Using default configuration..." << std::endl;
        gc = new GameCFG(640,480,16,irr::video::EDT_OPENGL,false);
    }
    setupEngine();
    TextureManager::getInstance()->initialize(driver);
    setupGameObjects();
}

App::~App()
{
    Logger::getInstance()->log("App Destructor",Logger::CTOR);
    device->drop();
}

void App::run()
{
    running = true;

    //update tasks (check if student solved)
    std::vector<TaskObject*> vt = scene->getTaskObjects();
    for (auto it = vt.begin(); it != vt.end(); ++it)
    {
        (*it)->update();
    }

    while(device->run() && running)
    {
        //store start time (to fps limit)
        auto startTime = std::chrono::high_resolution_clock::now();

        player->update();

        driver->beginScene(true, true, SColor(255,100,101,140));

        smgr->drawAll();
        guienv->drawAll();

        driver->endScene();

        //store time elapsed since rendering start
        std::chrono::duration<double, std::milli> elapsedTime = std::chrono::high_resolution_clock::now() - startTime;

        //if render is too fast then wait.. (17ms = (1/60)s = time for each frame = ~60fps)
        if(elapsedTime.count() > 17)
            std::this_thread::sleep_for(std::chrono::milliseconds(17 - (int)elapsedTime.count()));

        //exit with ESC
        if(eventReceiver->IsKeyDown(irr::KEY_ESCAPE))
            running = false;
    }
}

void App::setupEngine()
{
    eventReceiver = new GameEventReceiver();

    device = createDevice(  gc->getDriver(),
                            dimension2d<u32>(gc->getWidth(), gc->getHeight()),
                            gc->getDepth(),
                            gc->getFullscreen(),
                            false,
                            false,
                            eventReceiver);

    //custom logger
    Logger::getInstance()->setConfig(gc);

    //disable verbose irrlicht logs (allow only errors)
    device->getLogger()->setLogLevel(ELL_ERROR);

    device->setWindowCaption(L"Serious Game for Programming Students");

    driver = device->getVideoDriver();
    smgr = device->getSceneManager();
    guienv = device->getGUIEnvironment();
}

void App::setupGameObjects()
{
    scene = new GameScene(this, "scene1.txt");
    player = new GamePlayer(this);
    player->setPosition(scene->getPlayerStartPosition());
    setupCollisions();
}

void App::setupCollisions()
{
    //ground
    player->addCollision(scene->getGroundTriangleSelector());

    //static objects
    std::vector<StaticObject*> v = scene->getStaticObjects();
    for (auto it = v.begin(); it != v.end(); ++it)
    {
        ITriangleSelector* tri = (*it)->getTriangleSelector();
        if(tri!=NULL)
            player->addCollision( tri );
    }

    //task objects
    std::vector<TaskObject*> vt = scene->getTaskObjects();
    for (auto it = vt.begin(); it != vt.end(); ++it)
    {
        ITriangleSelector* tri = (*it)->getTriangleSelector();
        if(tri!=NULL)
            player->addCollision( tri );
    }
}
