#pragma once

class Shadow;

#include "App.hpp"

/**
* Shadow class. Defines the basic 3D-Shadow.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class Shadow
{
    public:
        Shadow() = default;
        /**
        * Main constructor.
        *
        * \param parent The scene node where the Shadow will be placed.
        * \param smgr The Scene Manager that contains this object (and his parent).
        * \param texture The string identifier to the shadow texture (quad shadow, round shadow, etc).
        * \param size The shadow size, default is 1.5.
        */
        Shadow(ISceneNode* parent, ISceneManager* smgr, std::string texture, float size=1.5f);

        /**
        * Copy constructor. (Rule of Three)
        * \param other another instance to be copied.
        */
        Shadow(const Shadow& other);

        /**
        * Destructor.
        */
        ~Shadow();

        /**
        * Copy assignment operator. (Rule of Three)
        * \param other another instance to be copied.
        */
        Shadow& operator= (const Shadow &other);

        /**
        * Shows and Hides the Shadow. (Rule of Three)
        *
        * \param visible True to show, false to hide.
        */
        void setVisible(bool visible);

        /**
        * Updates the parent.
        * \param parent another another 3D object.
        */
        void setParent(ISceneNode* parent);

        /**
        * Updates the shadow position.
        *
        * \param pos A 3-axis float vector (vector3df) with the new position.
        */
        void setPosition(vector3df pos);

        /**
        * Get the Scene Node that represents the shadow.
        *
        * \return The shadow Scene Node (engine stuff).
        */
        IMeshSceneNode* getNode();
    private:
        ISceneNode* parent;
        IMeshSceneNode* node;

        int shadowIndex;

        static int getNextIndex()
        {
            static int nextIndex = 0;
            return ++nextIndex;
        }
};
