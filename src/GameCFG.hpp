#pragma once
#include <string>
#include <irrlicht.h>

/**
* Class to represent Game configuration (Screen Size, Video Driver, etc).
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class GameCFG
{
    public:
        /**
        * Main constructor.
        *
        * \param filename a file to load config.
        */
        GameCFG(std::string filename);

        /**
        * Overloaded constructor. Allows to create a custom configuration without a file.
        *
        * \param screen_width screen width, e.g. 640
        * \param screen_height screen height, e.g. 480
        * \param screen_depth screen color depth: 32 or 16 bits.
        * \param driver video driver, from irrlicht supported drivers.
        * \param screen_width screen with, e.g. 640
        * \param fullscreen true to run in fullscreen, false to windowed.
        */
        GameCFG(int screen_width, int screen_height, int screen_depth, irr::video::E_DRIVER_TYPE driver, bool fullscreen);

        /**
        * Getter for screen width.
        *
        * \return screen width (pixels).
        */
        int getWidth();

        /**
        * Getter for screen height.
        *
        * \return screen height (pixels).
        */
        int getHeight();

        /**
        * Getter for screen color depth.
        *
        * \return screen depth (pixels).
        */
        int getDepth();

        /**
        * Getter for video driver.
        *
        * \return video driver (for irrlicht).
        */
        irr::video::E_DRIVER_TYPE getDriver();

        /**
        * Getter for fullscreen.
        *
        * \return true to use fullscreen, false to windowed.
        */
        bool getFullscreen();

        /**
        * Print logs for constructors?
        * \return true/false
        */
        bool logConstructors();

    private:
        int screen_width;
        int screen_height;
        int screen_depth;
        irr::video::E_DRIVER_TYPE driver;
        bool fullscreen;

        bool logConstruct;
};
