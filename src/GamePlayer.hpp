#pragma once

class GamePlayer;

#include "App.hpp"

/**
* Player class. Defines the user controlled character.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class GamePlayer
{
public:
    /**
    * Player constructor. Initializes the game character.
    *
    * \param app App object where this player will act.
    */
    GamePlayer(App* app);

    /**
    * Default destructor.
    */
    ~GamePlayer();

    /**
    * Add a triagle selector to the current player. All triangle selectors
    * will be used to block the player (e.g cannot walk through walls).
    *
    * \param selector Triangle selector of the other 3D object.
    */
    void addCollision(ITriangleSelector* selector);

    /**
    * Updates the player position.
    *
    * \param pos A 3-axis float vector (vector3df) with the new position.
    */
    void setPosition(vector3df pos);

    /**
    * Get the player position.
    *
    * \return pos A 3-axis float vector (vector3df) with the current position.
    */
    vector3df getPosition();

    /**
    * Update player actions, e.g. Walk if arrows are pressed.
    */
    void update();

private:
    const float WALK_SPEED = 0.1f;

    App* app;
    ICameraSceneNode* cam;
    IAnimatedMeshSceneNode* node;

    /**
    * Do all stuff related to player animations (walk cycle, look to left when
    * KEY_LEFT is pressed, etc).
    *
    * \param hMove Current horizontal movement (negative to go left, positive to go right).
    * \param vMove Current vertical movement (negative to go back, positive to go front).
    */
    void animate(int hMove, int vMove);

    /**
    * Given an arbitrary 3D point, make the player face to him.
    *
    * \param pos An arbitrary 3D point (vector3df).
    */
    void lookAt(vector3df pos);

    /**
    * Initializes the game Camera. the Camera is locked to the player,
    * so if the player moves then the camera follows him.
    */
    void setupCamera();

    /**
    * Initializes the 3D object that represents the player in the scene.
    */
    void setupMesh();
};
