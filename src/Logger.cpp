#include "Logger.hpp"

Logger::Logger(){}
Logger::~Logger(){}

Logger* Logger::getInstance()
{
	static Logger* instance = 0;
	if (!instance) instance = new Logger();
	return instance;
}

void Logger::setConfig(GameCFG* cg)
{
    this->cg = cg;
}

void Logger::log(std::string msg, int type)
{
    switch(type)
    {
        case GENERAL:
            std::cout << msg << std::endl;
            break;
        case CTOR:
            if(cg->logConstructors())
                std::cout << msg << std::endl;
            break;
    }
}
