#pragma once

class GameScene;

#include "App.hpp"
#include <vector>
#include <string>
#include "GameScene/StaticObjectsFactory.hpp"
#include "GameScene/TaskObjectsFactory.hpp"

using namespace std;

/**
* Game Scene class. Defines all the Static 3D Objects that composes the Scenery.
* Also defines the Tasks to be developed by the Student.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class GameScene
{
public:
    /**
    * Main constructor. Builds a scene based on a .txt file.
    * The .txt file must be writted using the following group of characters:
    *   1) ' ' (empty space)
    *   2) '#' creates a Wall.
    *   3) 'F' creates a Plant/Folliage.
    *   4) 'B' creates a Barrel Group.
    *   5) '1' (and other single character numbers) creates a Student Task (e.g 1 creates Task1, 2 creates Task2).
    *   6) 'P' sets the player start position.
    *
    * \param app App where this scene will be placed.
    * \param filename Game Scene .txt file.
    */
    GameScene(App* app, std::string filename);

    /**
    * Returns the start position of the Player.
    *
    * \return 3-axis float vector (vector3df).
    */
    vector3df getPlayerStartPosition();

    /**
    * Returns the ground triangle selector (collisions to ground are necessary).
    *
    * \return A triangle selector to be added to the player collisions list.
    */
    ITriangleSelector* getGroundTriangleSelector();

    /**
    * Returns a vector of all static objects in the Scenery.
    *
    * \return a vector of StaticObject*
    */
    std::vector<StaticObject*> getStaticObjects() const;

    /**
    * Returns a vector of all task objects in the Scenery.
    *
    * \return a vector of TaskObject*
    */
    std::vector<TaskObject*> getTaskObjects() const;

private:
    App* app;
    StaticObjectsFactory* staticObjectsFactory;
    TaskObjectsFactory* taskObjectsFactory;

    //scene data
    IMeshSceneNode* ground;
    vector<StaticObject*> staticObjects;
    vector<TaskObject*> taskObjects;
    vector3df playerStartPosition;

    /**
    * Reads a .txt file and builds the 3D scene.
    */
    void loadSceneFromFile(std::string filename);

    /**
    * Process a single character and builds the corresponding 3D object.
    *
    * \param objCode character which indicates the kind of object to be built.
    * \param x X position of the object.
    * \param z Z position of the object.
    */
    void processObject(char objCode, int x, int z);

    /**
    * Initializes the light for the Scenery.
    */
    void setupLights();
};
