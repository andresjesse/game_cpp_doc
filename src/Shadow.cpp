#include "Shadow.hpp"
#include "TextureManager.hpp"
#include "Logger.hpp"

Shadow::Shadow(ISceneNode* parent, ISceneManager* smgr, std::string texture, float size)
{
    node = smgr->addMeshSceneNode( smgr->getMesh("../media/shadow.b3d") );
    node->setMaterialTexture( 0, TextureManager::getInstance()->get(texture));
    node->setMaterialType(EMT_TRANSPARENT_ALPHA_CHANNEL);
    node->setPosition(vector3df(0,0.1f,0));
    node->setScale(vector3df(size,1,size));

    this->parent = parent;

    if (parent!=NULL)
        parent->addChild(node);

    shadowIndex = Shadow::getNextIndex();

    Logger::getInstance()->log("\tShadow? Default Constructor "+std::to_string(shadowIndex), Logger::CTOR);
}

//===================================Rule of Three
Shadow::Shadow(const Shadow& other)
{
    shadowIndex = Shadow::getNextIndex();
    //just clone other node and link to same parent
    //node = other.getNode()->clone();
    node = (IMeshSceneNode*)other.node->clone();
    parent = other.parent;
    Logger::getInstance()->log("\tCopying.."+std::to_string(other.shadowIndex), Logger::CTOR);
    Logger::getInstance()->log("\tShadow? Copy Constructor "+std::to_string(shadowIndex), Logger::CTOR);
}

Shadow::~Shadow()
{
    //node will be removed by his parent! can't delete or remove (smgr does that)
    Logger::getInstance()->log("\tShadow? Destructor "+std::to_string(shadowIndex), Logger::CTOR);
}

Shadow& Shadow::operator= (const Shadow &other)
{
    //just clone other node and link to same parent
    node = (IMeshSceneNode*)other.node->clone();
    parent = other.parent;
    Logger::getInstance()->log("\tShadow? Copy Assignment Operator", Logger::CTOR);
    return *this;
}
//===================================Rule of Three (end)

IMeshSceneNode* Shadow::getNode()
{
    return node;
}

void Shadow::setParent(ISceneNode* parent)
{
    //engine already does this..
    //if(this->parent != NULL)
    //    this->parent->removeChild(node);
    parent->addChild(node);
    this->parent = parent;
}

void Shadow::setVisible(bool visible)
{
    node->setVisible(visible);
}

void Shadow::setPosition(vector3df pos)
{
    node->setPosition(pos);
}
