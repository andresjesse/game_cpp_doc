#include "GamePlayer.hpp"

#include "TextureManager.hpp"
#include "Shadow.hpp"

GamePlayer::GamePlayer(App* app)
{
    this->app = app;
    setupCamera();
    setupMesh();
}

GamePlayer::~GamePlayer()
{

}

void GamePlayer::animate(int hMove, int vMove)
{
    if(hMove == 0 && vMove==0)
    {
        node->setFrameLoop(0,0);
        return;
    }

    if( node->getEndFrame()!=20 )
        node->setFrameLoop(2,20);

    lookAt(node->getPosition() + vector3df(-hMove,0,-vMove) );
}

void GamePlayer::lookAt(vector3df pos) {
    vector3df offsetVector = pos - node->getPosition();

    vector3df rot = (-offsetVector).getHorizontalAngle();

	if (rot.Y>361)
		rot.Y=rot.Y-360;

	if (rot.Y<-360)
		rot.Y=rot.Y+360;

    rot.X=0;
    rot.Z=0;

    node->setRotation(rot);
}

void GamePlayer::addCollision(ITriangleSelector* selector)
{
    node->addAnimator(app->smgr->createCollisionResponseAnimator(selector,node,vector3df(0.95f,1,0.95f),vector3df(0,0,0)));
}

void GamePlayer::setPosition(vector3df pos)
{
    node->setPosition(pos);
}

vector3df GamePlayer::getPosition()
{
    return node->getPosition();
}

void GamePlayer::update()
{
    //Store input
    float hMove = app->eventReceiver->IsKeyDown(irr::KEY_RIGHT) - app->eventReceiver->IsKeyDown(irr::KEY_LEFT);
    float vMove = app->eventReceiver->IsKeyDown(irr::KEY_UP) - app->eventReceiver->IsKeyDown(irr::KEY_DOWN);

    //update player position
    node->setPosition(node->getPosition()+vector3df( hMove*WALK_SPEED,0,vMove*WALK_SPEED ));

    //update camera position
    cam->setPosition(node->getPosition()+vector3df(0,10,-5));
    cam->setTarget(node->getPosition());

    //update player mesh rotation & animate
    animate(hMove,vMove);
}

void GamePlayer::setupCamera()
{
    cam = app->smgr->addCameraSceneNode(0, vector3df(0,10,-5));
}

void GamePlayer::setupMesh()
{
    //node = app->smgr->addMeshSceneNode( app->smgr->getMesh("../media/player.3ds") );
    node = app->smgr->addAnimatedMeshSceneNode( app->smgr->getMesh("../media/player/player.b3d") );
    node->setMaterialTexture( 0, TextureManager::getInstance()->get("robot") );
    node->setScale(vector3df(2,2,2));

    new Shadow(node, app->smgr, "round_shadow");
}
