#include "ConfigReadException.hpp"

ConfigReadException::ConfigReadException(std::string message)
{
    this->msg = "ConfigReadException: "+message;
}

const char* ConfigReadException::what() const throw()
{
    return msg.c_str();
}
