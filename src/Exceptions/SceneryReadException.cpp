#include "SceneryReadException.hpp"

SceneryReadException::SceneryReadException(std::string message)
{
    this->msg = "SceneryReadException: "+message;
}

const char* SceneryReadException::what() const throw()
{
    return msg.c_str();
}
