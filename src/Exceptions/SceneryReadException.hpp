#include <iostream>
#include <exception>
#include <string>

using namespace std;

/**
* Class that defines the main Exception for game
* scenery read related errors.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class SceneryReadException: public exception
{
public:
    /**
    * Main constructor. Can optionally receive an error
    * message to provide a better feedback.
    *
    * \param message string with the error message.
    */
    SceneryReadException(std::string message="");

    /**
    * Implementation of default exception what() method.
    *
    * \return a string that indicates Scenery Read Error plus the custom message (if provided in ctor).
    */
    virtual const char* what() const throw();
private:
    //custom message
    std::string msg;
};
