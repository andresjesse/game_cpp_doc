#include <iostream>
#include <exception>
#include <string>

using namespace std;

/**
* Class that defines the main Exception for game
* configuration read related errors.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class ConfigReadException: public exception
{
public:
    /**
    * Main constructor. Can optionally receive an error
    * message to provide a better feedback.
    *
    * \param message string with the error message.
    */
    ConfigReadException(std::string message="");

    /**
    * Implementation of default exception what() method.
    *
    * \return a string that indicates Config Read Error plus the custom message (if provided in ctor).
    */
    virtual const char* what() const throw();
private:
    //custom message
    std::string msg;
};
