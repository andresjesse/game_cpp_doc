#pragma once
#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace io;

/**
* Game event receiver. Looks for keyboard inputs and provides
* methods to external access this.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class GameEventReceiver : public IEventReceiver
{
public:
    /**
    * Override method. Register an event. This method is called
    * automatically by the engine.
    *
    * \param event All events produce SEvent objects, engine calls this method informing them.
    * \return true if the event was processed.
    */
    virtual bool OnEvent(const SEvent& event);

    /**
    * Override method. Verify if some key is pressed down.
    *
    * \param keyCode Code of the key to be verified. Check irr:: namespace, e.g irr::KEY_A, irr::KEY_B
    * \return true if the key is pressed down.
    */
    virtual bool IsKeyDown(EKEY_CODE keyCode) const;

    /**
    * Event receiver default constructor.
    */
    GameEventReceiver();

private:
    //array to store state of keys.
    bool KeyIsDown[KEY_KEY_CODES_COUNT];
};
