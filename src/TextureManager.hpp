#pragma once

#include <irrlicht.h>
#include <map>

using namespace irr;
using namespace video;

/**
* Texture Manager class. Defines all texture loading stuff.
* This class is implemented as a Singleton to guarantee access
* in the entire application.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class TextureManager
{
public:
    /**
    * Get Singleton instance.
    *
    * \return Always the same instance.
    */
    static TextureManager* getInstance();

    /**
    * Initializes the Textures by loading them with the Graphics Driver.
    *
    * \param driver Graphics Driver (provided by the engine).
    */
    void initialize(IVideoDriver* driver);

    /**
    * Get's a single texture based on his unique identifier.
    *
    * \param txName String identifier.
    * \return ITexture object pointer if texture exists, null otherwise.
    */
    ITexture* get(std::string txName);

private:
    /**
    * Private empty constructor, avoids to create instances.
    */
    TextureManager();

    /**
    * Private empty destructor.
    */
    ~TextureManager();

    //textures map.
    std::map<std::string,ITexture*> textures;
};
