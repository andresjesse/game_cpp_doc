#include "Task4.hpp"
#include <iostream>

int* Task4::generateSequence(int n)
{
    int* seq = (int*)malloc(sizeof(int)*4);

    int count = 0, i = n+1;

    while(count < 4)
    {
        if(n<1)
        {
            seq[count++] = -1;
            continue;
        }

        if(i%n == 0)
            seq[count++] = i;

        i++;
    }

    return seq;
}
