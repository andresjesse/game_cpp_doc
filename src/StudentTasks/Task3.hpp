#pragma once

#include "../TaskBase.hpp"
#include <string>

/**
* Student Task 3: Encrypt Password
*   Definition: Some users are not happy with their passwords, for some reason they
*   want to encrypt it. Write a code to help this task following this encrypt
*   rule:
*       Each character must be replaced with his sucessor, e.g. a -> b, f -> g, z -> a, etc.
*       Make sure to deal with 'z' character, it must be replaced by 'a'.
*       Consider only lowercase a-z characters are inserted (no need to check this!).
*
*   Header: string encrypt(string old_password);
*
*   Sample:
*       old_password = "abcl"
*       return: "bcdm"
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class Task3 : public TaskBase
{
public:
    /**
    * Default Constructor, leave it empty..
    */
    Task3(){};

    /**
    * Student code for task 3.
    */
    std::string encrypt(std::string old_password);
};
