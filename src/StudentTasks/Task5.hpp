#pragma once

#include "../TaskBase.hpp"
#include <string>

/**
* Student Task 5: Validate Card (Count Characters)
*   Definition: The last laboratory is the most restrict are in the scene,
*   so it requires a very hard code to allow access. This code is hidden inside
*   a character matrix of size 8x8. The matrix is composed by non-empty characters,
*   there's only one 'x' in there. You need to locate this special character ('x'),
*   after that, you need to count and return how many 'w' are around it (in a max
*   distance of 1 unit). You just need to count the characters and return, Your
*   mechanishm will be used to validate the cards.
*
*   Samples:
*     ########
*     #adfwhj#
*     #jshuaw#
*     #jawhhl#
*     #oxihal#
*     #uhwgta#
*     #qrakkw#
*     ########
*
*     - 'x' is at cell matrix[4][2]
*     - neighborhood (distance 1) of 'x' is:
*         jaw
*         oxi
*         uhw
*
*     - there's 2 'w' in neighborhood of 'x'
*
*     - pay attention that all matrices have the character '#' in the borders.
*
*   Header: int countCharacters(char matrix[8][8]);
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class Task5 : public TaskBase
{
public:
    /**
    * Default Constructor, leave it empty..
    */
    Task5(){};

    /**
    * Student code for task 5.
    */
    int countCharacters(char matrix[8][8]);
};
