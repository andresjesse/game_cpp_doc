#pragma once

#include "../TaskBase.hpp"
#include <string>

/**
* Student Task 2: Passwords for the Lab
*   Definition: Each user has his own username and password, the door opens
*               only for allowed users; The lab lock can handle with 5 users/passwords.
*   Header: bool verify_password_lab(string user, string password);
*   Expected result: user enters username and password, the function must return
*                    true if data match with the stored passwords, false otherwise.
*
*   The stored usernames/passwords are:
*       user1: volvo
*       user2: chevrolet
*       user3: audi
*       user4: bmw
*       user5: lada
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class Task2 : public TaskBase
{
public:
    /**
    * Default Constructor, leave it empty..
    */
    Task2(){};

    /**
    * Student code for task 2.
    */
    bool verifyPasswordLab(std::string username, std::string password);
};
