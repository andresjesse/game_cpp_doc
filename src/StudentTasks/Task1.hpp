#pragma once

#include "../TaskBase.hpp"
#include <iostream>

/**
* Student Task 1: Door with Password
*   Definition: the door only can be opened by inserting the correct password (1234);
*   Header: bool verify_password(int password);
*   Expected result: the method must return true if password is 1234, false otherwise.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class Task1 : public TaskBase
{
public:
    /**
    * Default Constructor, leave it empty..
    */
    Task1(){};

    /**
    * Student code for task 1.
    */
    bool verifyPassword(int password);

    /**
    * Operator Overload << (ostream to cout).
    */
    friend std::ostream &operator << (std::ostream &os, Task1* q);
};
