#include "Task3.hpp"

std::string Task3::encrypt(std::string old_password)
{
    std::string new_word = "";
    for(int i=0; i<(int)old_password.length(); i++)
    {
        new_word += 'a'+(((old_password[i]+1)-'a')%26);
    }
    return new_word;
}
