#include "Task5.hpp"
#include <iostream>

int Task5::countCharacters(char matrix[8][8])
{
    int result = 0;

    for(int i=0; i < 8;i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if(matrix[i][j]=='x')
			{
				for (int k = i-1; k <= i+1; k++)
				{
					for (int l = j-1; l <= j+1; l++)
					{
						if(matrix[k][l]=='w')
							result++;
					}
				}
			}
		}
	}

    return result;
}
