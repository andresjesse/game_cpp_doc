#include "GameCFG.hpp"
#include <fstream>
#include <streambuf>
#include <regex>
#include "Exceptions/ConfigReadException.hpp"

GameCFG::GameCFG(std::string filename)
{
    std::ifstream configFile("../"+filename);

    if(!configFile.is_open())
    {
        throw ConfigReadException("Can't open file:"+filename);
    }

    std::string str((std::istreambuf_iterator<char>(configFile)),
                    std::istreambuf_iterator<char>());

    std::smatch m;
    std::regex reW("SCREEN_WIDTH=([[:digit:]]+)");
    std::regex reH("SCREEN_HEIGHT=([[:digit:]]+)");
    std::regex reDp("SCREEN_DEPTH=([[:digit:]]+)");
    std::regex reDr("DRIVER=((OPENGL)|(SOFTWARE)|(BURNINGSVIDEO))");
    std::regex reF("FULLSCREEN=((false)|(true))");

    if (std::regex_search(str,m,reW))
        screen_width = std::atoi( m[1].str().c_str() );
    else
        throw ConfigReadException("Missing param: SCREEN_WIDTH");

    if (std::regex_search (str,m,reH))
        screen_height = std::atoi( m[1].str().c_str() );
    else
        throw ConfigReadException("Missing param: SCREEN_HEIGHT");

    if (std::regex_search (str,m,reDp))
        screen_depth = std::atoi( m[1].str().c_str() );
    else
        throw ConfigReadException("Missing param: SCREEN_DEPTH");

    if (std::regex_search (str,m,reDr))
    {
        driver = [](std::string driverName){
            if(driverName == "OPENGL")
                return irr::video::EDT_OPENGL;
            if(driverName == "SOFTWARE")
                return irr::video::EDT_SOFTWARE;
            //regex already evaluated strings.. no need to check last one
            return irr::video::EDT_BURNINGSVIDEO;
        }( m[1].str().c_str() );
    }
    else
        throw ConfigReadException("Missing param: DRIVER");

    if (std::regex_search (str,m,reF))
        fullscreen = [](std::string fs){
            return fs=="true";
        }( m[1].str().c_str() );
    else
        throw ConfigReadException("Missing param: FULLSCREEN");

    //Logger Config
    std::regex reCopyConstructors("LOG_CONSTRUCTORS=((false)|(true))");

    if (std::regex_search (str,m,reCopyConstructors))
        logConstruct = [](std::string result){
            return result=="true";
    }( m[1].str().c_str() );

    configFile.close();
}

GameCFG::GameCFG(int screen_width, int screen_height, int screen_depth, irr::video::E_DRIVER_TYPE driver, bool fullscreen)
{
    this->screen_width = screen_width;
    this->screen_height = screen_height;
    this->screen_depth = screen_depth;
    this->driver = driver;
    this->fullscreen = fullscreen;
}


int GameCFG::getWidth()
{
    return this->screen_width;
}

int GameCFG::getHeight()
{
    return this->screen_height;
}

int GameCFG::getDepth()
{
    return this->screen_depth;
}

irr::video::E_DRIVER_TYPE GameCFG::getDriver()
{
    return this->driver;
}

bool GameCFG::getFullscreen()
{
    return this->fullscreen;
}

bool GameCFG::logConstructors()
{
    return logConstruct;
}
