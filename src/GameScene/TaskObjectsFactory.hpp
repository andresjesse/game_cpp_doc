#pragma once

class TaskObjectsFactory;

#include "TaskObjects/TaskObject.hpp"

/**
* Task Objects Factory. Provides an easy interface to build and
* configure Task Objects.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class TaskObjectsFactory
{
    public:
        /**
        * Main constructor.
        *
        * \param mgr scene manager to contain/manage the new 3D object.
        */
        TaskObjectsFactory(ISceneManager* smgr);

        /**
        * Creates an instance of Task1 in the Scenery. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new Task Object.
        */
        TaskObject* createTask1(vector3df pos);

        /**
        * Creates an instance of Task2 in the Scenery. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new Task Object.
        */
        TaskObject* createTask2(vector3df pos);

        /**
        * Creates an instance of Task3 in the Scenery. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new Task Object.
        */
        TaskObject* createTask3(vector3df pos);

        /**
        * Creates an instance of Task4 in the Scenery. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new Task Object.
        */
        TaskObject* createTask4(vector3df pos);

        /**
        * Creates an instance of Task5 in the Scenery. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new Task Object.
        */
        TaskObject* createTask5(vector3df pos);
    private:
        //Scene Manager
        ISceneManager* smgr;
};
