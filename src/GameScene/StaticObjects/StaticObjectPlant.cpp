#include "StaticObjectPlant.hpp"

#include "../TextureManager.hpp"
#include "../Shadow.hpp"
#include "../Logger.hpp"

StaticObjectPlant::StaticObjectPlant(ISceneManager* mgr)
{
    smgr=mgr;

    mesh=smgr->getMesh("../media/objects/plant1.b3d");
    node=smgr->addMeshSceneNode(mesh);
    node->setMaterialTexture(0, TextureManager::getInstance()->get("plant1") );
    node->setMaterialType(EMT_TRANSPARENT_ALPHA_CHANNEL);
    node->setScale(vector3df(1.5f));

    Logger::getInstance()->log("StaticObjectPlant ? ",Logger::CTOR);

    static Shadow baseShadow = Shadow(node, smgr, "round_shadow");

    //using copy assignment operator
    Shadow temp = baseShadow;

    //assign to instance var
    shadow = &temp;

    shadow->setParent(node);
}
