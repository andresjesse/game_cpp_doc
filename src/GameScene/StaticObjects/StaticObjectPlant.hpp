#pragma once

#include "../App.hpp"
#pragma once

#include "StaticObject.hpp"

/**
* Specialized static object. Represents a 3D plant/folliage.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class StaticObjectPlant: public StaticObject
{
public:
    /**
    * Main constructor for this static object.
    *
    * \param mgr scene manager to contain/manage the new 3D object.
    */
    StaticObjectPlant(ISceneManager* mgr);
};
