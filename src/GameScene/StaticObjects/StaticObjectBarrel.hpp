#pragma once

#include "../App.hpp"
#include "StaticObject.hpp"

/**
* Specialized static object. Represents a 3D barrels group.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class StaticObjectBarrel : public StaticObject
{
public:
    /**
    * Main constructor for this static object.
    *
    * \param mgr scene manager to contain/manage the new 3D object.
    */
    StaticObjectBarrel(ISceneManager* mgr);
};
