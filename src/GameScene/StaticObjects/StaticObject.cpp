#include "StaticObject.hpp"

void StaticObject::setPosition(vector3df pos)
{
    node->setPosition(pos);
}

vector3df StaticObject::getPosition()
{
    return node->getPosition();
}

ITriangleSelector* StaticObject::getTriangleSelector()
{
    return node->getTriangleSelector();
}
