#include "StaticObjectBarrel.hpp"

#include "../TextureManager.hpp"
#include "../Shadow.hpp"
#include "../../Logger.hpp"

StaticObjectBarrel::StaticObjectBarrel(ISceneManager* mgr)
{
    smgr=mgr;

    mesh=smgr->getMesh("../media/objects/barrel.b3d");
    node=smgr->addMeshSceneNode(mesh);
    node->setMaterialTexture(0, TextureManager::getInstance()->get("barrel") );

    node->setTriangleSelector(smgr->createTriangleSelectorFromBoundingBox(node));

    Logger::getInstance()->log("StaticObjectBarrel ? ",Logger::CTOR);

    new Shadow(node, smgr, "round_shadow",2.0f);
}
