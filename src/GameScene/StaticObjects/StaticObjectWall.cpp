#include "StaticObjectWall.hpp"

#include "../TextureManager.hpp"
#include "../Shadow.hpp"
#include "../Logger.hpp"

StaticObjectWall::StaticObjectWall(ISceneManager* mgr)
{
    smgr=mgr;

    mesh=smgr->getMesh("../media/objects/box.b3d");
    node=smgr->addMeshSceneNode(mesh);
    node->setMaterialTexture(0, TextureManager::getInstance()->get("wood_box") );

    node->setTriangleSelector(smgr->createTriangleSelectorFromBoundingBox(node));

    Logger::getInstance()->log("StaticObjectWall ? ",Logger::CTOR);
    static Shadow* baseShadow = new Shadow(NULL, smgr, "quad_shadow", 1.4f);
    baseShadow->setVisible(false);

    shadow = new Shadow(*baseShadow);
    shadow->setParent(node);
    shadow->setVisible(true);

    //new Shadow(node, smgr, "quad_shadow", 1.4f);
}
