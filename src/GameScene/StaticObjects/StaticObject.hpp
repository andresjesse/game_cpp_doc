#pragma once

#include <irrlicht.h>

class StaticObject;

#include "../../Shadow.hpp"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

/**
* Base class for all Static 3D Objects that compose the Scenery.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class StaticObject
{
    public:
        /**
        * Updates the object's position in 3D space.
        *
        * \param pos A float 3-sized (x,y,z) vector with the new position.
        */
        void setPosition(vector3df pos);

        /**
        * Get the object's position in 3D space.
        *
        * \return A float 3-sized (x,y,z) vector with the current position.
        */
        vector3df getPosition();

        /**
        * Get the triangle selector, used to calculate collisions.
        *
        * \return triangle selector associated with the current object (in general based on boundingbox).
        */
        ITriangleSelector* getTriangleSelector();

    protected:
        //scene manager that contains current object
        ISceneManager* smgr;

        //3D mesh and Scene Node
        IMesh* mesh;
        IMeshSceneNode* node;

        //load first, clone next ones (copy constructor)
        Shadow* shadow;
};
