#include "StaticObjectsFactory.hpp"

#include "StaticObjects/StaticObjectWall.hpp"
#include "StaticObjects/StaticObjectPlant.hpp"
#include "StaticObjects/StaticObjectBarrel.hpp"

StaticObjectsFactory::StaticObjectsFactory(ISceneManager* smgr)
{
    this->smgr = smgr;
}

StaticObject* StaticObjectsFactory::createWall(vector3df pos)
{
    StaticObjectWall* obj = new StaticObjectWall(smgr);
    obj->setPosition(pos);
    return obj;
}

StaticObject* StaticObjectsFactory::createPlant(vector3df pos)
{
    StaticObjectPlant* obj = new StaticObjectPlant(smgr);
    obj->setPosition(pos);
    return obj;
}

StaticObject* StaticObjectsFactory::createBarrel(vector3df pos)
{
    StaticObjectBarrel* obj = new StaticObjectBarrel(smgr);
    obj->setPosition(pos);
    return obj;
}
