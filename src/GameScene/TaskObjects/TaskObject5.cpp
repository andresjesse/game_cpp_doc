#include "TaskObject5.hpp"

#include "../StudentTasks/Task5.hpp"
#include "../../Logger.hpp"

#include <memory>

TaskObject5::TaskObject5(ISceneManager* smgr, std::string taskOk, std::string taskLocked)
: TaskObject(smgr)
{
    Logger::getInstance()->log("TaskObject5 ? ",Logger::CTOR);

    setupMesh();
    setupLabel(taskOk, taskLocked);
    attachStudentTask(new Task5());
}

bool TaskObject5::evalStudentWork()
{
    Task5* task = (Task5*)studentTask;

    auto check = [](Task5* task, char matrix[8][8], int expectedResult){
        return task->countCharacters(matrix) == expectedResult;
    };

    //Sample matrix (result=2)
    char matrix1[8][8] ={{'#','#','#','#','#','#','#','#'},
                         {'#','a','d','f','w','h','j','#'},
                         {'#','j','s','h','u','a','w','#'},
                         {'#','j','a','w','h','h','l','#'},
                         {'#','o','x','i','h','a','l','#'},
                         {'#','u','h','w','g','t','a','#'},
                         {'#','q','r','a','k','k','w','#'},
                         {'#','#','#','#','#','#','#','#'}};
    if(!check(task, matrix1, 2)) return false;

    //w-matrix (result=8)
    char matrix2[8][8] ={{'#','#','#','#','#','#','#','#'},
                         {'#','w','w','w','w','w','w','#'},
                         {'#','w','w','w','w','x','w','#'},
                         {'#','w','w','w','w','w','w','#'},
                         {'#','w','w','w','w','w','w','#'},
                         {'#','w','w','w','w','w','w','#'},
                         {'#','w','w','w','k','w','w','#'},
                         {'#','#','#','#','#','#','#','#'}};
    if(!check(task, matrix2, 8)) return false;

    //matrix3 (result=0)
    char matrix3[8][8] ={{'#','#','#','#','#','#','#','#'},
                         {'#','x','d','f','w','h','j','#'},
                         {'#','j','s','h','u','a','w','#'},
                         {'#','j','a','w','h','h','l','#'},
                         {'#','o','g','i','h','a','l','#'},
                         {'#','u','h','w','g','t','a','#'},
                         {'#','a','q','g','k','k','w','#'},
                         {'#','#','#','#','#','#','#','#'}};
    if(!check(task, matrix3, 0)) return false;

    return true;
}
