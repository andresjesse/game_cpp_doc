#pragma once

#include "TaskObject.hpp"

/**
* Specialized Task Object. Represents a 3D obstacle in the Scenery,
* shown when student don't completed task 5 yet.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class TaskObject5 : public TaskObject
{
public:
    /**
    * Main constructor.
    *
    * \param mgr Scene Manger to contain/manage the 3D object.
    * \param taskOk positive feedback message (task solved).
    * \param taskLocked negative feedback message (task unsolved).
    */
    TaskObject5(ISceneManager* mgr, std::string taskOk, std::string taskLocked);

protected:
    /**
    * Implementation of evaluation method. This contains positive and
    * negative tests to evaluate student solution for Task 5.
    *
    * \return true if student solved correctly, false otherwise.
    */
    virtual bool evalStudentWork();
};
