#include "TaskObject.hpp"
#include <iostream>

#include "../TextureManager.hpp"

TaskObject::TaskObject(ISceneManager* smgr)
{
    this->smgr = smgr;
}

TaskObject::~TaskObject()
{
    smgr->addToDeletionQueue(node);
}

void TaskObject::attachStudentTask(TaskBase* newTask)
{
    this->studentTask = newTask;
}

void TaskObject::setPosition(vector3df pos)
{
    node->setPosition(pos);
    okShadow->setPosition(pos+vector3df(0,0.1f,0));
}

ITriangleSelector* TaskObject::getTriangleSelector()
{
    return node->getTriangleSelector();
}

void TaskObject::update()
{
    if(evalStudentWork() == false)
    {
        node->setPosition(vector3df(node->getPosition().X,0,node->getPosition().Z));
        okShadow->setVisible(false);
    }
    else
    {
        node->setPosition(vector3df(node->getPosition().X,-10,node->getPosition().Z));
        okShadow->setVisible(true);
    }
}

void TaskObject::setupLabel(std::string taskOK, std::string taskLocked)
{
    IGUIEnvironment *gui = smgr->getGUIEnvironment();
    IGUIFont* font = gui->getFont("../media/bigfont.png");
    bText = smgr->addBillboardTextSceneNode(font,
                                            stringw(taskLocked.c_str()).c_str(),
                                            node,
                                            dimension2df(5.0f,0.8f));

    bText->setPosition( vector3df(0,3.5f,0) );

    bOkText = smgr->addBillboardTextSceneNode(font,
                                            stringw(taskOK.c_str()).c_str(),
                                            okShadow->getNode(),
                                            dimension2df(5.0f,0.8f));

    bOkText->setPosition( vector3df(0,3.5f,0) );
}

void TaskObject::setupMesh()
{
    IMesh* mesh=smgr->getMesh("../media/objects/task.b3d");
    node=smgr->addMeshSceneNode(mesh);
    node->setMaterialTexture(0, TextureManager::getInstance()->get("task") );

    node->setTriangleSelector(smgr->createTriangleSelectorFromBoundingBox(node));

    new Shadow(node, smgr, "task_shadow", 1.4f);
    okShadow = new Shadow(NULL, smgr, "ok_shadow", 1.4f);
}
