#include "TaskObject4.hpp"
#include "../../Logger.hpp"

#include "../StudentTasks/Task4.hpp"

#include <memory>

TaskObject4::TaskObject4(ISceneManager* smgr, std::string taskOk, std::string taskLocked)
: TaskObject(smgr)
{
    Logger::getInstance()->log("TaskObject4 ? ",Logger::CTOR);

    setupMesh();
    setupLabel(taskOk, taskLocked);
    attachStudentTask(new Task4());
}

bool TaskObject4::evalStudentWork()
{
    Task4* task = (Task4*)studentTask;

    auto check = [](Task4* task, int n){
        std::unique_ptr<int[]> seq(task->generateSequence(n));

        for(int i=0; i<4; i++)
            if(seq[i] != (i+2)*n)
                return false;
        return true;
    };

    //positive tests
    std::vector<int> positiveTests{2,5,16};

    for(auto n:positiveTests)
        if(!check(task, n))
            return false;

    //negative tests
    std::vector<int> negativeTests{0,-14};

    for(auto n:negativeTests)
    {
        if(! [](Task4* task, int n){
                std::unique_ptr<int[]> seq(task->generateSequence(n));
                for(int i=0;i<4;i++){
                    if(seq[i] != -1)
                        return false;}
                return true;
            }(task,n)
        )
            return false;
    }

    /*
    //old way (to compare)

    //positive tests
    int* seq = task->generateSequence(2);
    for(int i=0; i<4; i++)
        if(seq[i] != (i+2)*2)
            return false;

    seq = task->generateSequence(5);
    for(int i=0; i<4; i++)
        if(seq[i] != (i+2)*5)
            return false;

    seq = task->generateSequence(16);
    for(int i=0; i<4; i++)
        if(seq[i] != (i+2)*16)
            return false;

    //negative number/zero tests
    seq = task->generateSequence(0);
    for(int i=0; i<4; i++)
        if(seq[i] != -1)
            return false;

    seq = task->generateSequence(-14);
    for(int i=0; i<4; i++)
        if(seq[i] != -1)
            return false;
    */

    return true;
}
