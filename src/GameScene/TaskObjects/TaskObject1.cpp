#include "TaskObject1.hpp"

#include "../TextureManager.hpp"
#include "../Shadow.hpp"
#include "../../Logger.hpp"

#include "../StudentTasks/Task1.hpp"

TaskObject1::TaskObject1(ISceneManager* smgr, std::string taskOk, std::string taskLocked)
: TaskObject(smgr)
{
    Logger::getInstance()->log("TaskObject1 ? ",Logger::CTOR);

    setupMesh();

    setupLabel(taskOk, taskLocked);

    attachStudentTask(new Task1());
}

bool TaskObject1::evalStudentWork()
{
    Task1* task = (Task1*)studentTask;

    //std::cout << "eval============================" << task << std::endl;


    //negative test
    if(task->verifyPassword(1000) == true)
        return false;

    //positive test
    if(task->verifyPassword(1234) == false)
        return false;

    return true;
}
