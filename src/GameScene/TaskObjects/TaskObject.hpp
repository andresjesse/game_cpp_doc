#pragma once

#include <irrlicht.h>
#include <string>
#include "../TaskBase.hpp"

class TaskObject;

#include "../Shadow.hpp"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

/**
* Base class to Task Objects. Task Objects are used as a container to the
* Student Tasks (that are much simpler to avoid expose complex game details).
* Also, this kind of objects are shown in the Scenery in two ways: (1) as
* a red locked obstacle (player cannot pass) or (2) free green way (for solved
* tasks).
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class TaskObject
{
    public:
        /**
        * Main constructor.
        *
        * \param mgr scene manager to contain/manage the new 3D object.
        */
        TaskObject(ISceneManager* smgr);

        /**
        * Default destructor.
        */
        virtual ~TaskObject();

        /**
        * This method links a Student Task 'newTask' to the 3D object.
        * This way the path can be free or locked according to the solution
        * provided by the student to the task 'newTask'.
        *
        * \param newTask Any Student Task (must be subclass of TaskBase).
        */
        void attachStudentTask(TaskBase* newTask);

        /**
        * Get the triangle selector, used to calculate collisions.
        *
        * \return triangle selector associated with the current object (in general based on boundingbox).
        */
        ITriangleSelector* getTriangleSelector();

        /**
        * Updates the object's position in 3D space.
        *
        * \param pos A float 3-sized (x,y,z) vector with the new position.
        */
        void setPosition(vector3df pos);

        /**
        * Updates the 3D Object state: allowing passage (green stage) or
        * locking the path (red stage, player cannot pass). This is step is
        * done always before game main loop.
        */
        void update();

    protected:
        //Scene Manager and Visual Objects
        ISceneManager* smgr;
        ISceneNode* node;
        Shadow* okShadow;
        //Labels for positive and negative feedback
        IBillboardTextSceneNode* bText;
        IBillboardTextSceneNode* bOkText;
        //Student Task attached to this instance
        TaskBase* studentTask;

        /**
        * Setup the two labels for this object: (1) positive feedback,
        * shown when student correctly solved the task and (2) negative
        * feedback, shown when answer is not correct.
        *
        * \param taskOk positive feedback message.
        * \param taskLocked negative feedback message.
        */
        void setupLabel(std::string taskOk, std::string taskLocked);

        /**
        * Setup the 3D meshes to represent this object and his stages.
        */
        void setupMesh();

        /**
        * Virtual method to evaluate the student solution. This must
        * be implemented on subclasses that know the Student Task and
        * his valid/invalid inputs and outputs.
        */
        virtual bool evalStudentWork() = 0;
};
