#include "TaskObject3.hpp"
#include "../../Logger.hpp"

#include "../StudentTasks/Task3.hpp"

TaskObject3::TaskObject3(ISceneManager* smgr, std::string taskOk, std::string taskLocked)
: TaskObject(smgr)
{
    Logger::getInstance()->log("TaskObject3 ? ",Logger::CTOR);

    setupMesh();
    setupLabel(taskOk, taskLocked);
    attachStudentTask(new Task3());
}

bool TaskObject3::evalStudentWork()
{
    Task3* task = (Task3*)studentTask;
    std::string result;

    //negative tests
    result = task->encrypt("aabb");
    if( result == "aabb" )
        return false;

    if( result == "" )
        return false;

    //positive tests
    if(result != "bbcc")
        return false;

    result = task->encrypt("xyz");
    if(result != "yza")
        return false;

    result = task->encrypt("zzzaa");
    if(result != "aaabb")
        return false;

    return true;
}
