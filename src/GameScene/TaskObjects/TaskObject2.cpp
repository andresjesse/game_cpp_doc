#include "TaskObject2.hpp"
#include "../../Logger.hpp"

#include "../StudentTasks/Task2.hpp"

TaskObject2::TaskObject2(ISceneManager* smgr, std::string taskOk, std::string taskLocked)
: TaskObject(smgr)
{
    Logger::getInstance()->log("TaskObject2 ? ",Logger::CTOR);

    setupMesh();
    setupLabel(taskOk, taskLocked);
    attachStudentTask(new Task2());
}

bool TaskObject2::evalStudentWork()
{
    Task2* task = (Task2*)studentTask;
    //negative tests
    if(task->verifyPasswordLab("user1","chevrolet") == true)
        return false;
    if(task->verifyPasswordLab("user2","volvo") == true)
        return false;
    if(task->verifyPasswordLab("user3","chevrolet") == true)
        return false;
    if(task->verifyPasswordLab("user4","volvo") == true)
        return false;
    if(task->verifyPasswordLab("user5","chevrolet") == true)
        return false;

    //positive tests
    if(task->verifyPasswordLab("user1","volvo") == false)
        return false;
    if(task->verifyPasswordLab("user2","chevrolet") == false)
        return false;
    if(task->verifyPasswordLab("user3","audi") == false)
        return false;
    if(task->verifyPasswordLab("user4","bmw") == false)
        return false;
    if(task->verifyPasswordLab("user5","lada") == false)
        return false;

    return true;
}
