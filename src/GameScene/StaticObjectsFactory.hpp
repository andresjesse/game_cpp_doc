#pragma once

class StaticObjectsFactory;

#include "StaticObjects/StaticObject.hpp"

/**
* Static Objects Factory. Provides an easy interface to build and
* configure Static 3D Objects.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class StaticObjectsFactory
{
    public:
        /**
        * Main constructor.
        *
        * \param mgr scene manager to contain/manage the new 3D object.
        */
        StaticObjectsFactory(ISceneManager* smgr);

        /**
        * Creates a 3D Wall (box based). Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new 3D object.
        */
        StaticObject* createWall(vector3df pos);

        /**
        * Creates a 3D Plant/Folliage. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new 3D object.
        */
        StaticObject* createPlant(vector3df pos);

        /**
        * Creates a 3D Barrel Group. Also translate it to the provided position.
        *
        * \param pos position of the new object in game Scenery (x,y,z).
        * \return a reference to the new 3D object.
        */
        StaticObject* createBarrel(vector3df pos);
    private:
        //Scene manager
        ISceneManager* smgr;
};
