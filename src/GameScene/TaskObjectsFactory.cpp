#include "TaskObjectsFactory.hpp"

#include "TaskObjects/TaskObject1.hpp"
#include "../StudentTasks/Task1.hpp"

#include "TaskObjects/TaskObject2.hpp"
#include "../StudentTasks/Task2.hpp"

#include "TaskObjects/TaskObject3.hpp"
#include "../StudentTasks/Task3.hpp"

#include "TaskObjects/TaskObject4.hpp"
#include "../StudentTasks/Task4.hpp"

#include "TaskObjects/TaskObject5.hpp"
#include "../StudentTasks/Task5.hpp"

TaskObjectsFactory::TaskObjectsFactory(ISceneManager* smgr)
{
    this->smgr = smgr;
}

TaskObject* TaskObjectsFactory::createTask1(vector3df pos)
{
    TaskObject1* obj = new TaskObject1(smgr, "Task 1 Solved!", "Solve the Task 1!");
    obj->setPosition(pos);
    return obj;
}

TaskObject* TaskObjectsFactory::createTask2(vector3df pos)
{
    TaskObject2* obj = new TaskObject2(smgr, "Task 2 Solved!", "Solve the Task 2!");
    obj->setPosition(pos);
    return obj;
}

TaskObject* TaskObjectsFactory::createTask3(vector3df pos)
{
    TaskObject3* obj = new TaskObject3(smgr, "Task 3 Solved!", "Solve the Task 3!");
    obj->setPosition(pos);
    return obj;
}

TaskObject* TaskObjectsFactory::createTask4(vector3df pos)
{
    TaskObject4* obj = new TaskObject4(smgr, "Task 4 Solved!", "Solve the Task 4!");
    obj->setPosition(pos);
    return obj;
}

TaskObject* TaskObjectsFactory::createTask5(vector3df pos)
{
    TaskObject5* obj = new TaskObject5(smgr, "Task 5 Solved!", "Solve the Task 5!");
    obj->setPosition(pos);
    return obj;
}
