#include "TextureManager.hpp"

TextureManager::TextureManager(){}

TextureManager::~TextureManager(){}

TextureManager* TextureManager::getInstance()
{
	static TextureManager *instance = 0;
	if (!instance) instance = new TextureManager();
	return instance;
}

void TextureManager::initialize(IVideoDriver* driver)
{
    //ground
    textures["asphalt"] = driver->getTexture("../media/asphalt.jpg");
    textures["grass"] = driver->getTexture("../media/grass.jpg");

    //static objects
    textures["wood_box"] = driver->getTexture("../media/objects/wood_box.jpg");
    textures["brick1"] = driver->getTexture("../media/objects/brick1.jpg");
    textures["plant1"] = driver->getTexture("../media/objects/plant1.png");
    textures["barrel"] = driver->getTexture("../media/objects/barrel.jpg");

    //other objects
    textures["task"] = driver->getTexture("../media/objects/task.jpg");

    //player
    textures["robot"] = driver->getTexture("../media/player/robot.jpg");

    //shadows
    textures["round_shadow"] = driver->getTexture("../media/round_shadow.png");
    textures["quad_shadow"] = driver->getTexture("../media/quad_shadow.png");
    textures["task_shadow"] = driver->getTexture("../media/task_shadow.png");
    textures["ok_shadow"] = driver->getTexture("../media/ok_shadow.png");
}

ITexture* TextureManager::get(std::string txName)
{
    return textures[txName];
    //TODO:throw exception TextureNotFound
}
