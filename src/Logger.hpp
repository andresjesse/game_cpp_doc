#pragma once
#include <iostream>

class Logger;

#include "GameCFG.hpp"

using namespace std;

class Logger
{
public:
    static const int GENERAL = 0;
    static const int CTOR = 1;

    /**
    * Get Singleton instance.
    *
    * \return Always the same instance.
    */
    static Logger* getInstance();

    /**
    * Set game configuration.
    *
    * \param cg Config used in main app.
    */
    void setConfig(GameCFG* cg);

    /**
    * Prints a log.
    *
    * \param msg string message to be printed.
    * \param type type of the log, use one contant value from this class.
    */

    void log(std::string msg, int type=GENERAL);

private:
    /**
    * Private empty constructor, avoids to create instances.
    */
    Logger();

    /**
    * Private empty destructor.
    */
    ~Logger();

    GameCFG* cg;
};
