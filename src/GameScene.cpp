#include <irrlicht.h>
#include "GameScene.hpp"

#include <fstream>
#include <algorithm>
#include <iostream>

#define BLOCK_SIZE 2

#include "TextureManager.hpp"
#include "Exceptions/SceneryReadException.hpp"

GameScene::GameScene(App* app, std::string filename)
{
    this->app = app;
    staticObjectsFactory = new StaticObjectsFactory(app->smgr);
    taskObjectsFactory = new TaskObjectsFactory(app->smgr);

    try{
        loadSceneFromFile(filename);
    }catch(SceneryReadException& ex){
        std::cout << ex.what() << std::endl;
        exit(1);
    }

    setupLights();

    ground = app->smgr->addMeshSceneNode(app->smgr->getMesh("../media/plane20.3ds"));
    ground->setMaterialTexture( 0, TextureManager::getInstance()->get("asphalt") );
    ground->setScale(vector3df(100,1,100));
}

ITriangleSelector* GameScene::getGroundTriangleSelector()
{
    return ground->getTriangleSelector();
}

vector3df GameScene::getPlayerStartPosition()
{
    return playerStartPosition;
}

std::vector<StaticObject*> GameScene::getStaticObjects() const
{
    return staticObjects;
}

std::vector<TaskObject*> GameScene::getTaskObjects() const
{
    return taskObjects;
}

void GameScene::loadSceneFromFile(std::string filename)
{
    ifstream sceneFile("../media/scenes/"+filename);

    if(!sceneFile.is_open())
    {
        throw SceneryReadException("Can't open file:"+filename);
    }

    std::string line;
    int z = 0;
    while (std::getline(sceneFile, line))
    {
        for(std::string::size_type x = 0; x < line.size(); ++x) {
            processObject(line[x],x,z);
        }
        ++z;
    }
    sceneFile.close();
}

void GameScene::processObject(char objCode, int x, int z)
{
    switch(objCode){
    case ' ':
        break;
    case '#':
        staticObjects.push_back( staticObjectsFactory->createWall(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case 'F':
        staticObjects.push_back( staticObjectsFactory->createPlant(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case 'B':
        staticObjects.push_back( staticObjectsFactory->createBarrel(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case '1':
        taskObjects.push_back( taskObjectsFactory->createTask1(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case '2':
        taskObjects.push_back( taskObjectsFactory->createTask2(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case '3':
        taskObjects.push_back( taskObjectsFactory->createTask3(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case '4':
        taskObjects.push_back( taskObjectsFactory->createTask4(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case '5':
        taskObjects.push_back( taskObjectsFactory->createTask5(vector3df(x*BLOCK_SIZE,0,z*BLOCK_SIZE)) );
        break;
    case 'P':
        playerStartPosition.set(x*BLOCK_SIZE,0.2,z*BLOCK_SIZE);
        break;
    default:
        std::string msg("Invalid character: ");
        msg+=objCode;
        throw SceneryReadException(msg);
    break;
    }
}

void GameScene::setupLights()
{
    app->smgr->addLightSceneNode(0, vector3df(0,100,100),SColorf(1,1,1,1),1000);
    app->smgr->setAmbientLight(SColor(200,200,200,255));
}
