#pragma once

class App;

#include <irrlicht.h>
#include <iostream>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#include "GameEventReceiver.hpp"
#include "GamePlayer.hpp"
#include "GameScene.hpp"
#include "GameCFG.hpp"

/**
* Main class of the game. Setups the engine, scene and does the main loop.
*
* \author Andres Jesse Porfirio (www.andresjesse.com)
*/
class App
{
    //declare friend classes
    friend class GameScene;
    friend class GamePlayer;
    friend class TextureManager;
public:
    /**
    * Main App construtor. Initializes the game engine, textures and 3D objects.
    */
    App();

    /**
    * Default destructor, drops the game engine (which drops everything else).
    */
    ~App();

    /**
    * Executes the game.
    */
    void run();

protected:
    //Game engine stuff
    GameEventReceiver* eventReceiver;
    IrrlichtDevice* device;
    IVideoDriver* driver;
    ISceneManager* smgr;
    IGUIEnvironment* guienv;

private:
    bool running;
    GameCFG* gc;
    GamePlayer* player;
    GameScene* scene;

    /**
    * Initializes the game engine (irrlicht 3D).
    */
    void setupEngine();

    /**
    * Initializes the 3D objects.
    */
    void setupGameObjects();

    /**
    * Initializes collisions between the Player and the Scenery.
    */
    void setupCollisions();
};
